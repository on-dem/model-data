Bodies data
___________

This page displays only attributes added at a certain inheritance level. For seeing all attributes of each data type see `this variant <body_full.html>`_


.. automodapi:: body
   :no-main-docstr:
   :no-heading:
   :skip: SomeStateClass, SomeMaterialClass, SomeShapeClass, Vector3, Matrix3, Quaternion
  
.. .. inheritance-diagram:: body.box body.sphere body.polyhedron
..    :top-classes: body.shape

.. .. automod-diagram:: body
..    :skip: Vector3, Matrix3, Quaternion
..    
.. .. automodapi:: body
..    :no-heading:
..    :no-inheritance-diagram:
..    :no-main-docstr:
..    :skip: SomeStateClass, SomeMaterialClass, SomeShapeClass, Vector3, Matrix3, Quaternion
