Scene
_____

Information on a simulation

.. automodapi:: scene
   :no-main-docstr:
   :no-inheritance-diagram:
   :no-heading:
   :skip: SomeStateClass, SomeMaterialClass, SomeShapeClass, Vector3, Matrix3, Quaternion
   
Detailed content
----------------
   
.. autoclass:: scene
