Interactions data
_________________

This page displays only attributes added at a certain inheritance level. For seeing all attributes of each data type see `this variant <interaction_full.html>`_

.. .. automod-diagram:: interaction
..    :skip: SomeStateClass, SomeMaterialClass, SomeShapeClass, Vector3, Matrix3, Quaternion
   
.. automodapi:: interaction
   :no-main-docstr:
   :no-heading:
   :skip: SomeStateClass, SomeMaterialClass, SomeShapeClass, Vector3, Matrix3, Quaternion
   
.. .. automodapi:: interaction
..    :no-heading:
..    :no-inheritance-diagram:
..    :no-main-docstr:
..    :skip: SomeStateClass, SomeMaterialClass, SomeShapeClass, Vector3, Matrix3, Quaternion
